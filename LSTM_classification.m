clear all
close all
clc

path = 'D:\Python Code\IQ_Option\data18\';
files = dir(strcat(path,'*.csv'));

target_asset_id = 4;

attach = [];
for i = 1:25
        %files(i).name
        Tem = xlsread(strcat(path,files(i).name));
        data = Tem(:,1:4);
        attach = [attach;data'];
end


files(target_asset_id).name
Tem = xlsread(strcat(path,files(target_asset_id).name));
data = Tem(:,1:4);

data = data';
op = data(1,:)';
hi = data(2,:)';
lo = data(3,:)';
cl = data(4,:)';
vo = ones(1,length(cl))';
price = cl;

roc                  = indicators(price           ,'roc'    ,1)';

X = [roc]';
label = sign(X(2:end,1));

X = [op';hi';lo';cl']';
X = [X attach'];

X = X(1:end-1,:);
X = X(30:end,:);
label = label(30:end);

for i = 1:size(X,2)
    X(:,i) = (X(:,i)-min(X(:,i)))/(max(X(:,i))-min(X(:,i)));
end
data = X';
label = label';
Num_data = size(data,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lookback = 10;
Thre_mov = 0;
Len_Val = lookback+60;
numTimeStepsTrain = 0+300;
dataTrain = data(:,1+0:numTimeStepsTrain+1);
YTrain = label(1+0:numTimeStepsTrain+1)';
YVal = label(1+0:numTimeStepsTrain+1)';
dataTest = data(:,numTimeStepsTrain+1:numTimeStepsTrain+150);
YTest = label(numTimeStepsTrain+1:numTimeStepsTrain+150)';

dataTrainStandardized = dataTrain(:,1:end-Len_Val);
YTrain = YTrain(1:end-Len_Val);
YTrain = YTrain(1+lookback-1:end-1);

c1 = 1;
for  i = lookback:size(dataTrainStandardized(:,1:end-1),2)
    if YTrain(i-lookback+1)~=0 && abs(YTrain(i-lookback+1))>=Thre_mov
        XTrain(c1,:) = mat2cell(dataTrainStandardized(:,i-lookback+1:i),[Num_data],[lookback]);
        c1 = c1 + 1;
    end
end
YTrain(find(YTrain==0))=[];
YTrain(find(abs(YTrain)<Thre_mov))=[];
YTrain = sign(YTrain);
YTrain = categorical(YTrain);
dataValStandardized = dataTrain(:,end-Len_Val+1:end);
YVal = YVal(end-Len_Val+1:end);
YVal = YVal(1+lookback-1:end-1);

c1 = 1;
for  i = lookback:size(dataValStandardized(:,1:end-1),2)
    if YVal(i-lookback+1)~=0 && abs(YVal(i-lookback+1))>=Thre_mov
        XVal(c1,:) = mat2cell(dataValStandardized(:,i-lookback+1:i),[Num_data],[lookback]);
        c1 = c1 + 1;
    end
end
YVal(find(YVal==0))=[];
YVal(find(abs(YVal)<Thre_mov))=[];
YVal = sign(YVal);
YVal = categorical(YVal);
inputSize = Num_data;
numHiddenUnits = 10;
numClasses = 2;

layers = [ ...
    sequenceInputLayer(inputSize)
    bilstmLayer(numHiddenUnits,'OutputMode','last')
    fullyConnectedLayer(numClasses)
    softmaxLayer
    classificationLayer]
maxEpochs = 1500;
miniBatchSize = 100;

options = trainingOptions('adam', ...
    'ExecutionEnvironment','cpu', ...
    'GradientThreshold',1, ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'ValidationData',{XVal,YVal},...
    'SequenceLength','longest', ...
    'Shuffle','never', ...
    'Verbose',0, ...
    'Plots','training-progress');

net = trainNetwork(XTrain,YTrain,layers,options);

dataTestStandardized = dataTest;

YTest = YTest(1+lookback-1:end-1);
c1 = 1;
for  i = lookback:size(dataTestStandardized(:,1:end-1),2)
    if YTest(i-lookback+1)~=0 && abs(YTest(i-lookback+1))>=Thre_mov
        XTest(c1,:) = mat2cell(dataTestStandardized(:,i-lookback+1:i),[Num_data],[lookback]);
        c1 = c1 + 1;
    end
end
YPred = classify(net,XTest, ...
    'MiniBatchSize',miniBatchSize, ...
    'SequenceLength','longest');

YTest(find(YTest==0))=[];
YTest(find(abs(YTest)<Thre_mov))=[];
YTest = sign(YTest);
YTest = categorical(YTest);
number_of_trades = numel(YTest)
acc = 100*sum(YPred == YTest)./numel(YTest)

figure
subplot(2,1,1)
plot(YTest)
hold on
plot(YPred,'.-')
hold off
legend(["Observed" "Forecast"])
ylabel("Cases")
title("Forecast")

