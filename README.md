# forex_prediction

The matlab file in this repository is used to predict forex price movement.
It uses the information about the current and past price movements of 25 currencies and predict the next step price movement of a target currency.
The price movement is defined in a binary format where 1 represent up movement and -1 represent down movement.
Deep LSTM model is used to predict such a binary movement. 